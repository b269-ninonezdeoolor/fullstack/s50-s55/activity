import Banner       from '../components/Banner';
import Highlights   from '../components/Highlights';

export default function Home() 
{
    return (
        <div>
            <Banner 
                title="Zuitt Coding Bootcamp" 
                subtitle="Opportunities for everyone, everywhere." 
                button="Enroll now!" 
                destination="/courses" 
                backgroundColor="white"
            />
            <Highlights/>
        </div>
    );
}