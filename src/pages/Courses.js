import { useState, useEffect }  from 'react';
import CourseCard               from '../components/CourseCard';


export default function Courses()
{
    // to store the courses retrieved from the database
    const [courses, setCourses] = useState([]);

    useEffect(() => 
    {
		fetch(`${process.env.REACT_APP_API_URL}/courses/active`)
		.then(res => res.json())
		.then(data => 
        {
			console.log(data);
			setCourses(data.map(course => 
            {
				return(
					<CourseCard key={course._id} course={course} />
				)
			}))
		})
	}, [])

    return (
        <>
            {courses}
        </>
    )
}

// PROPS
// Way to declare props drillling
// We can pass information from one compnent to another using props (props drilling)
// Curly braces {} are used for props to signify that we are providing/passing information from one component to another using JS expression