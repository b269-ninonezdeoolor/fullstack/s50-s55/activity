import Banner from '../components/Banner';


export default function Error() 
{
    return (
      <Banner 
      title="Error 404 - Page not found" 
      subtitle="The page you are looking for cannot be found." 
      button="Back to Home" 
      destination="/login" 
      backgroundColor="#ccc" />
    );
}