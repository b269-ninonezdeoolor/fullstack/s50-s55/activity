import { Row, Col, Button } from 'react-bootstrap';
import { Link }  from 'react-router-dom';


export default function Banner({ title, subtitle, button, destination, backgroundColor }) 
{
  // const { title, subtitle, button, destination, backgroundColor } = data;

    return (
      <Row className="banner" style={{ backgroundColor: backgroundColor }}>
        <Col className="p-5">
          <h1>{ title }</h1>
          <p>{ subtitle }</p>
          <Button variant="dark" as={Link} to={ destination }>{ button }</Button>
        </Col>
      </Row>
    );
}