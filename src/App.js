import AppNavbar                                  from './components/AppNavbar';
import CourseView                                 from './components/CourseView';
import Error                                      from './components/Error';
import Home                                       from './pages/Home';
import Courses                                    from "./pages/Courses";
import Register                                   from "./pages/Register";
import Login                                      from "./pages/Login";
import Logout                                     from './pages/Logout';
import { Container }                              from 'react-bootstrap';
import { UserProvider }                           from './UserContext';
import { useState, useEffect }                    from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import './App.css';


export default function App() 
{
  const [user, setUser] = useState({
    id: null, 
    isAdmin: null
  });

  const unsetUser = () => 
  {
    localStorage.clear();
  }

  useEffect(() => 
  {
    fetch(`${process.env.REACT_APP_API_URL}/user/details`, 
    {
      method: 'GET',
      headers: 
      {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data => 
      {
        // User is logged in
        if(typeof data.id !== 'undefined') 
        {
          setUser({
            id: data.id, 
            isAdmin: data.isAdmin
          })
        }
        // User is logged out
        else 
        {
          setUser({
            id: null, 
            isAdmin: null
          })
        }
      })
  }, []);

  return (
    // <></> - common pattern in React for component to return multiple elements
    <>
      <UserProvider value={{user, setUser, unsetUser}}>
        <Router>
          <AppNavbar/>
          <Container>
            <Routes>
              <Route path="/"                   element={<Home />} />
              <Route path="/courses"            element={<Courses />} />
              <Route path="/courses/:courseId"  element={<CourseView />} />
              <Route path="/register"           element={<Register />} />
              <Route path="/login"              element={<Login />} />
              <Route path="/logout"             element={<Logout />} />
              <Route path="/*"                  element={<Error />} />
            </Routes>
          </Container>
        </Router>
      </UserProvider>
    </>
  );
}